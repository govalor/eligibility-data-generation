﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MegaModel;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;
using API_Library_2._0;

namespace EligibilityDataGen.con
{
    class Program
    {

        public static void post(CMStudentGradesModel.RootObject root)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/CMStudentGrades");
            client.Authenticator = new HttpBasicAuthenticator("cheyanne.miller@nextworld.net", "Pion33rs!");

            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", JsonConvert.SerializeObject(root), ParameterType.RequestBody); // adds to POST or URL querystring based on Method

            // easy async support
            client.ExecuteAsync(request, response =>
            {
                Console.WriteLine(response.Content);
            });
            Console.WriteLine();
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            //data generation
            GenerateRecord generator = new GenerateRecord();
            ModelTranslation translator = new ModelTranslation();

            Model.RootObject model = generator.getModel(3);

            List<CMStudentGradesModel.RootObject> gradeReports = translator.toEligibilityReport(model);

            //print the reports
            //translator.printEligibilityReport(gradeReports);
            //Console.ReadLine();

            //Post each root object (AKA one student) at a time
            CMStudentGradesModel.RootObject rootToPost;

            for (int i = 0; i < gradeReports.Count(); i++)
            {
                rootToPost = gradeReports.ElementAt(i);

                post(rootToPost);
            }
            Console.ReadLine();

            //testing the api stuff
            //SchoologyEligibility apiGen = new SchoologyEligibility();

            //List<API_Library_2._0.StudentGradesModel> grades = new List<API_Library_2._0.StudentGradesModel>();

            //grades = apiGen.getGradesByGroup("Hockey");

            //Console.WriteLine("Name: " + grades.ElementAt(2).FirstName);
            //Console.ReadLine();

        }
    }
}
