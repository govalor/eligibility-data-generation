﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Translation_Models
{
    class NWLeaderFundDetail
    {
        public class Name
        {
            public string NameName { get; set; }
            public string FormatUsed { get; set; }
            public string NameKeyNames { get; set; }
            public string NameNameInverted { get; set; }
            public string NameNamesBeforeKeyNames { get; set; }
        }

        public class Address
        {
            public string FormatUsed { get; set; }
            public string AddressCity { get; set; }
            public string AddressFull { get; set; }
            public string AddressLine1 { get; set; }
            public string AddressState { get; set; }
            public string AddressCountry { get; set; }
            public string AddressPostalCode { get; set; }
        }

        public class AppData
        {
            public string nwId { get; set; }
            public Name Name { get; set; }
            public string Notes { get; set; }
            public Address Address { get; set; }
            public string JVEmail { get; set; }
            public double JVFunds { get; set; }
            public DateTime DateTime { get; set; }
            public bool znwLocked { get; set; }
            public int JVLeaderID { get; set; }
            public string Description { get; set; }
            public double JVGiftTotal { get; set; }
            public string PhoneNumber { get; set; }
            public int JVLineItemID { get; set; }
            public string JVContributor { get; set; }
            public int JVSolicitorID { get; set; }
            public string JVCityStateZip { get; set; }
            public int JVExperienceID { get; set; }
            public bool JVDonorAnonymous { get; set; }
            public bool JVAmountAnonymous { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
        }


        public class UserInterfaceHint
        {
            public string TableSchema { get; set; }
            public object Field { get; set; }
            public object HeaderDetail { get; set; }
            public object SubTableField { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<UserInterfaceHint> UserInterfaceHints { get; set; }
        }

        public class PageData
        {
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
            public PageData pageData { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
