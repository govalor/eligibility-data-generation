﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Final_Models
{
    public class GetStudents_Result
    {
        public int EA7RecordsID { get; set; }
        public string UserDefinedID { get; set; }
        public string NickName { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string GradeLevel { get; set; }
        //public short ClassOf { get; set; }
        public int ClassOf { get; set; }
        public string Email { get; set; }
    }
}
