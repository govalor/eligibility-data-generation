﻿using API_Library_2._0.Final_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class ValorAD
    {
        //authentication client
        HttpBasicAuthenticator authentication = new HttpBasicAuthenticator("Cheyanne.Miller@nextworld.net", "Pion33rs!");

        public ValorAD(){}

        public List<StudentModel> getADStudents()
        {
            List<StudentModel> list = new List<StudentModel>();

            //Execute Get request to retrieve data from Nextworld
            StudentGetModel.RootObject getRoot = new StudentGetModel.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMActiveDirectory");
            client.Authenticator = authentication;
            
            var request = new RestRequest(Method.GET);

            //to deserialize the response -use for production
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<StudentGetModel.RootObject>(response.Content);

            //translate into appropriate model
            list = toStudentModel(getRoot.Data.records);

            return list;
        }

        public List<StudentModel> toStudentModel(List<StudentGetModel.Record> list)
        {
            List<StudentModel> newList = new List<StudentModel>();
            StudentModel newStudent;
            StudentGetModel.AppData oldStudent;

            for (int i = 0; i < list.Count(); i++)
            {
                newStudent = new StudentModel();
                oldStudent = list.ElementAt(i).appData;

                newStudent.EA7RecordsID = oldStudent.CMEA7RecordsID;
                newStudent.StudentID = oldStudent.CMStudentIDNumber;
                newStudent.NickName = oldStudent.CMNickName;
                newStudent.Title = oldStudent.CMTitle;
                newStudent.FirstName = oldStudent.CMFirstName;
                newStudent.MiddleName = oldStudent.CMMiddleName;
                newStudent.LastName = oldStudent.CMLastName;
                newStudent.Gender = oldStudent.CMGender;
                newStudent.GradeLevel = oldStudent.CMGradeLevel;
                newStudent.OUDefaultGroup = oldStudent.CMOUDefaultGroup;
                newStudent.OUClassGroup = oldStudent.CMOUClassGroup;
                newStudent.OULocation = oldStudent.CMOULocation;
                newStudent.Email = oldStudent.CMEmail;
                newStudent.Password = oldStudent.CMPassword;

                newList.Add(newStudent);
            }

            return newList;
        }

        public List<FacultyStaffModel> getADFacultyStaff(String option)
        {
            List<FacultyStaffModel> list = new List<FacultyStaffModel>();

            //Execute Get request to retrieve data from Nextworld
            FacultyGetModel.RootObject getRoot = new FacultyGetModel.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMFacultyAD");
            client.Authenticator = authentication;

            var request = new RestRequest(Method.GET);

            //to deserialize the response -use for production
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<FacultyGetModel.RootObject>(response.Content);

            //translate into appropriate model
            list = toFacultyStaffModel(option, getRoot.Data.records);

            return list;
        }

        public List<FacultyStaffModel> toFacultyStaffModel(String type, List<FacultyGetModel.Record> toTranslate)
        {
            List<FacultyStaffModel> list = new List<FacultyStaffModel>();
            FacultyStaffModel newFaculty;
            FacultyGetModel.AppData oldFaculty;
            String testType = "";

            for (int i = 0; i < toTranslate.Count(); i++)
            {
                oldFaculty = toTranslate.ElementAt(i).appData;

                //filters to only retrieve the indicated type of AD record: Faculty or Staff
                testType = "Valor " + type;
                if (oldFaculty.CMOUDefaultGroup.Equals(testType))
                {
                    newFaculty = new FacultyStaffModel();

                    newFaculty.EA7RecordsID = oldFaculty.CMEA7RecordsID;
                    newFaculty.FacultyID = oldFaculty.CMFacultyID;
                    newFaculty.NickName = oldFaculty.CMNickName;
                    newFaculty.FirstName = oldFaculty.CMFirstName;
                    newFaculty.LastName = oldFaculty.CMLastName;
                    newFaculty.Gender = oldFaculty.CMGender;
                    newFaculty.OULocation = oldFaculty.CMOULocation;
                    newFaculty.OUDefaultGroup = oldFaculty.CMOUDefaultGroup;
                    newFaculty.OUClassGroup = oldFaculty.CMOUClassGroup;
                    newFaculty.Email = oldFaculty.CMEmail;
                    newFaculty.Password = oldFaculty.CMPassword;

                    list.Add(newFaculty);
                }
            }
            return list;
        }
    }
}
