﻿using API_Library_2._0.Final_Models;
using API_Library_2._0.Translation_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    
    public class SchoologySchedule
    {
        public static HttpBasicAuthenticator auth = new HttpBasicAuthenticator("jake.vossen@nextworld.net", "gmAHF7eS9!BI$20MzFkk0nW3qz&LdXykwUs!RSBq");
        public static List<GetStudents_Result> GetStudents()
        {
            List<GetStudents_Result> temp = new List<GetStudents_Result>();

            StudentNW.RootObject allStudentsFromNW = new StudentNW.RootObject();

            allStudentsFromNW = getAllStudentsRootObjectAPI("https://master-api1.nextworld.net/v2/JVGetStudentsResult");

            temp = convertAllStudentsToOldModle(allStudentsFromNW);
            //logger.Debug("Converted the NW model to the old model");
            return temp;
        }

        private static List<GetStudents_Result> convertAllStudentsToOldModle(StudentNW.RootObject allStudentsFromNW)
        {
            List<GetStudents_Result> temp = new List<GetStudents_Result>();

            List<StudentNW.Record> records = new List<StudentNW.Record>();
            records = allStudentsFromNW.Data.records;

            for (int i = 0; i < records.Count; i++)
            {
                temp.Add(convertStudentToOldModle(records.ElementAt(i).appData));
            }

            return temp;
        }

        private static GetStudents_Result convertStudentToOldModle(StudentNW.AppData appData)
        {
            GetStudents_Result temp = new GetStudents_Result();

            temp.ClassOf = appData.JVClassOf;
            temp.EA7RecordsID = appData.JVEA7RecordsID;
            temp.Email = appData.JVEmail;
            temp.FirstName = appData.JVFirstName;
            temp.Gender = appData.JVGender;
            temp.GradeLevel = appData.JVGradeLevel;
            temp.LastName = appData.JVLastName;
            temp.MiddleName = appData.JVMiddleName;
            temp.NickName = appData.JVNickName;
            temp.Title = appData.JVTitle;
            temp.UserDefinedID = appData.JVUserDefinedID;

            return temp;
        }

        private static StudentNW.RootObject getAllStudentsRootObjectAPI(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            client.Authenticator = auth;

            StudentNW.RootObject root = new StudentNW.RootObject();

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            root = JsonConvert.DeserializeObject<StudentNW.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return root;
        }

        //public virtual ObjectResult<GetStudentSchedule_Result> GetStudentSchedule(Nullable<int> eA7RecordsId)
        //{
        //    var eA7RecordsIdParameter = eA7RecordsId.HasValue ?
        //        new ObjectParameter("EA7RecordsId", eA7RecordsId) :
        //        new ObjectParameter("EA7RecordsId", typeof(int));

        //    return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetStudentSchedule_Result>("GetStudentSchedule", eA7RecordsIdParameter);
        //}

        public static List<GetStudentSchedule_Result> GetStudentSchedule(Nullable<int> eA7RecordsId)
        {
            List<GetStudentSchedule_Result> temp = new List<GetStudentSchedule_Result>();
            CoursesNWGET.RootObject root = generateStudentSchedule(eA7RecordsId);

            List<CoursesNWGET.AppData> appDatas = new List<CoursesNWGET.AppData>();

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                appDatas.Add(root.Data.records.ElementAt(i).appData);
            }

            GetStudentSchedule_Result tempSchedule = new GetStudentSchedule_Result();

            foreach (CoursesNWGET.AppData appData in appDatas)
            {
                tempSchedule = new GetStudentSchedule_Result();
                tempSchedule.BlockColor = appData.JVBlockColor;
                tempSchedule.BlockColorRGB = appData.JVBlockColorRGB;
                tempSchedule.ClassName = appData.JVClassName;
                tempSchedule.Course = appData.JVCourse;
                tempSchedule.CourseSection = appData.JVCourseSection;
                tempSchedule.CycleDaysID = appData.JVCycleDaysID;
                tempSchedule.Day = appData.JVDay;
                tempSchedule.EndTime = appData.JVEndTime;
                tempSchedule.EndTimeRaw = new System.DateTime(1, 1, 1, 0, appData.JVEndTimeRaw, 0);
                tempSchedule.Faculty = appData.JVFaculty;
                tempSchedule.Room = appData.JVRoom;
                tempSchedule.Semester = appData.JVSemester;
                tempSchedule.StartTime = appData.JVStartTime;
                // tempSchedule.StartTimeRaw = new System.DateTime(1, 1, 1, 0, appData.JVStartTimeRaw, 0);
                temp.Add(tempSchedule);
            }


            return temp;
        }

        private static CoursesNWGET.RootObject generateStudentSchedule(int? eA7RecordsId)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVGetStudentScheduleResult?nwFilter=%7B%22$and%22:%5B%7B%22JVEA7RecordsID%22:%7B%22$gte%22:" + eA7RecordsId + "%7D%7D,%7B%22JVEA7RecordsID%22:%7B%22$lte%22:" + eA7RecordsId + "%7D%7D%5D%7D");
            var request = new RestRequest(Method.GET);
            client.Authenticator = auth;

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response.Content);
        }

        private static CoursesNWGET.RootObject generateStudentSchedule(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            client.Authenticator = auth;

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response.Content);
        }

        // Old method that uses blackbaud and SQL
        //public virtual ObjectResult<GetTeachers_Result> GetTeachers()
        //{
        //    return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetTeachers_Result>("GetTeachers");
        //}

        public static List<GetTeachers_Result> GetTeachers()
        {
            List<GetTeachers_Result> temp = new List<GetTeachers_Result>();

            TeacherNW.RootObject root = getAllTeacherRootObject("https://master-api1.nextworld.net/v2/JVSchoologyTeachersResult/");

            temp = convertRootObjectTeacherIntoModel(root);

            return temp;
        }

        private static List<GetTeachers_Result> convertRootObjectTeacherIntoModel(TeacherNW.RootObject root)
        {
            List<GetTeachers_Result> temp = new List<GetTeachers_Result>();

            List<TeacherNW.Record> records = new List<TeacherNW.Record>();
            records = root.Data.records;

            for (int i = 0; i < records.Count; i++)
            {
                temp.Add(covertTeacherRecordtoOldModel(records.ElementAt(i).appData));
            }

            return temp;
        }

        private static GetTeachers_Result covertTeacherRecordtoOldModel(TeacherNW.AppData appData)
        {
            GetTeachers_Result temp = new GetTeachers_Result();

            temp.Department = appData.JVDepartment;
            temp.EA7RecordsID = appData.JVEA7RecordsID;
            temp.Email = appData.EmailAddress;
            temp.FacultyID = appData.JVFacultyID;
            temp.FirstName = appData.JVFirstName;
            temp.LastName = appData.JVLastName;
            temp.SchoolID = appData.JVSchoolIDForFacutly;

            return temp;
        }

        private static TeacherNW.RootObject getAllTeacherRootObject(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            client.Authenticator = auth;

            TeacherNW.RootObject root = new TeacherNW.RootObject();

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            root = JsonConvert.DeserializeObject<TeacherNW.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return root;
        }


        // Old one with SQL
        //public virtual ObjectResult<GetTeacherSchedule_Result> GetTeacherSchedule(Nullable<int> eA7RecordsId)
        //{
        //    var eA7RecordsIdParameter = eA7RecordsId.HasValue ?
        //        new ObjectParameter("EA7RecordsId", eA7RecordsId) :
        //        new ObjectParameter("EA7RecordsId", typeof(int));

        //    return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetTeacherSchedule_Result>("GetTeacherSchedule", eA7RecordsIdParameter);
        //}

        // Jake Vossen read NW applicaiton
        public static List<GetTeacherSchedule_Result> GetTeacherSchedule(Nullable<int> eA7RecordsId)
        {
            List<GetTeacherSchedule_Result> temp = new List<GetTeacherSchedule_Result>();

            TeacherScheduleNW.RootObject root = generateTeacherSchedule(eA7RecordsId);

            temp = convertRootObjectTeacherScheduleIntoModel(root);

            return temp;
        }

        private static TeacherScheduleNW.RootObject generateTeacherSchedule(int? eA7RecordsId)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVGetTeacherScheduleResult?nwFilter=%7B%22$and%22:%5B%7B%22JVEA7RecordsID%22:%7B%22$gte%22:" + eA7RecordsId + "%7D%7D,%7B%22JVEA7RecordsID%22:%7B%22$lte%22:" + eA7RecordsId + "%7D%7D%5D%7D");
            var request = new RestRequest(Method.GET);
            client.Authenticator = auth;

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response.Content);
        }
        private static TeacherScheduleNW.RootObject generateTeacherSchedule(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            client.Authenticator = auth;

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response.Content);
        }

        private static List<GetTeacherSchedule_Result> convertRootObjectTeacherScheduleIntoModel(TeacherScheduleNW.RootObject root)
        {
            List<GetTeacherSchedule_Result> temp = new List<GetTeacherSchedule_Result>();

            List<TeacherScheduleNW.Record> records = new List<TeacherScheduleNW.Record>();
            records = root.Data.records;

            for (int i = 0; i < records.Count; i++)
            {
                temp.Add(covertTeacherScheduleRecordtoOldModel(records.ElementAt(i).appData));
            }

            return temp;
        }

        private static GetTeacherSchedule_Result covertTeacherScheduleRecordtoOldModel(TeacherScheduleNW.AppData appData)
        {
            GetTeacherSchedule_Result temp = new GetTeacherSchedule_Result();

            temp.BlockColor = appData.JVBlockColor;
            temp.BlockColorRGB = appData.JVBlockColorRGB;
            temp.ClassName = appData.JVClassName;
            temp.Course = appData.JVCourse;
            temp.CourseSection = appData.JVCourseSection;
            temp.CycleDaysID = appData.JVCycleDaysID;
            temp.Day = appData.JVDay;
            temp.EndTime = appData.JVEndTime;
            //temp.EndTimeRaw = new System.DateTime(1, 1, 1, 0,appData.JVEndTimeRaw, 0);
            temp.StartTime = appData.JVStartTime;
            //temp.StartTimeRaw = new System.DateTime(1, 1, 1, 0, appData.JVEndTimeRaw, 0);

            return temp;
        }

        // Old Method With SQL
        //public virtual ObjectResult<GetAllStudentsSchedule_Result> GetAllStudentsSchedule()
        //{
        //    return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAllStudentsSchedule_Result>("GetAllStudentsSchedule");
        //}

        public static List<GetAllStudentsSchedule_Result> GetAllStudentsSchedule()
        {
            List<GetAllStudentsSchedule_Result> temp = new List<GetAllStudentsSchedule_Result>();

            CoursesNWGET.RootObject root = generateStudentSchedule("https://master-api1.nextworld.net/v2/JVGetStudentScheduleResult");

            GetAllStudentsSchedule_Result tempStudentsAllSchedule = new GetAllStudentsSchedule_Result();

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                tempStudentsAllSchedule = new GetAllStudentsSchedule_Result();
                tempStudentsAllSchedule.Course = root.Data.records.ElementAt(i).appData.JVCourse;
                tempStudentsAllSchedule.BlockColor = root.Data.records.ElementAt(i).appData.JVBlockColor;
                tempStudentsAllSchedule.BlockColorRGB = root.Data.records.ElementAt(i).appData.JVBlockColorRGB;
                tempStudentsAllSchedule.ClassName = root.Data.records.ElementAt(i).appData.JVClassName;
                tempStudentsAllSchedule.CourseSection = root.Data.records.ElementAt(i).appData.JVCourseSection;
                tempStudentsAllSchedule.CycleDaysID = root.Data.records.ElementAt(i).appData.JVCycleDaysID;
                tempStudentsAllSchedule.UserUniqueID = root.Data.records.ElementAt(i).appData.JVEA7RecordsID;
                tempStudentsAllSchedule.Day = root.Data.records.ElementAt(i).appData.JVDay;
                tempStudentsAllSchedule.EndTime = root.Data.records.ElementAt(i).appData.JVEndTime;
                tempStudentsAllSchedule.Faculty = root.Data.records.ElementAt(i).appData.JVFaculty;
                tempStudentsAllSchedule.Room = root.Data.records.ElementAt(i).appData.JVRoom;
                tempStudentsAllSchedule.Semester = root.Data.records.ElementAt(i).appData.JVSemester;
                tempStudentsAllSchedule.StartTime = root.Data.records.ElementAt(i).appData.JVStartTime;
                temp.Add(tempStudentsAllSchedule);
            }

            return temp;
        }

        // Old method with SQL
        //public virtual ObjectResult<GetAllTeachersSchedule_Result> GetAllTeachersSchedule()
        //{
        //    return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAllTeachersSchedule_Result>("GetAllTeachersSchedule");
        //}

        public static List<GetAllTeachersSchedule_Result> GetAllTeachersSchedule()
        {
            List<GetAllTeachersSchedule_Result> temp = new List<GetAllTeachersSchedule_Result>();

            TeacherScheduleNW.RootObject root = generateTeacherSchedule("https://master-api1.nextworld.net/v2/JVGetTeacherScheduleResult");
            GetAllTeachersSchedule_Result tempTeachersAllSchedule = new GetAllTeachersSchedule_Result();

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                tempTeachersAllSchedule = new GetAllTeachersSchedule_Result();
                tempTeachersAllSchedule.Course = root.Data.records.ElementAt(i).appData.JVCourse;
                tempTeachersAllSchedule.BlockColor = root.Data.records.ElementAt(i).appData.JVBlockColor;
                tempTeachersAllSchedule.BlockColorRGB = root.Data.records.ElementAt(i).appData.JVBlockColorRGB;
                tempTeachersAllSchedule.ClassName = root.Data.records.ElementAt(i).appData.JVClassName;
                tempTeachersAllSchedule.CourseSection = root.Data.records.ElementAt(i).appData.JVCourseSection;
                tempTeachersAllSchedule.CycleDaysID = root.Data.records.ElementAt(i).appData.JVCycleDaysID;
                tempTeachersAllSchedule.Day = root.Data.records.ElementAt(i).appData.JVDay;
                tempTeachersAllSchedule.EA7RecordsID = root.Data.records.ElementAt(i).appData.JVEA7RecordsID;
                tempTeachersAllSchedule.EndTime = root.Data.records.ElementAt(i).appData.JVEndTime;
                tempTeachersAllSchedule.Faculty = root.Data.records.ElementAt(i).appData.JVFaculty;
                tempTeachersAllSchedule.Room = root.Data.records.ElementAt(i).appData.JVRoom;
                tempTeachersAllSchedule.Semester = root.Data.records.ElementAt(i).appData.JVSemester;
                tempTeachersAllSchedule.StartTime = root.Data.records.ElementAt(i).appData.JVStartTime;
                temp.Add(tempTeachersAllSchedule);
            }

            return temp;
        }

        public static List<GetStudentScheduleBySemester_Result> GetStudentScheduleBySemester(Nullable<int> eA7RecordsId, Nullable<int> semester)
        {
            // fall = 1
            // spring = 2
            List<GetStudentScheduleBySemester_Result> temp = new List<GetStudentScheduleBySemester_Result>();

            List<GetStudentSchedule_Result> allClasses = GetStudentSchedule(eA7RecordsId);
            GetStudentScheduleBySemester_Result tempSchedule = new GetStudentScheduleBySemester_Result();
            for (int i = 0; i < allClasses.Count; i++)
            {
                if (semester == 1)
                {
                    if (allClasses.ElementAt(i).Semester.ToLower().Equals("fall"))
                    {
                        tempSchedule = new GetStudentScheduleBySemester_Result();
                        tempSchedule.BlockColor = allClasses.ElementAt(i).BlockColor;
                        tempSchedule.BlockColorRGB = allClasses.ElementAt(i).BlockColorRGB;
                        tempSchedule.ClassName = allClasses.ElementAt(i).ClassName;
                        tempSchedule.Course = allClasses.ElementAt(i).Course;
                        tempSchedule.CourseSection = allClasses.ElementAt(i).CourseSection;
                        tempSchedule.CycleDaysID = allClasses.ElementAt(i).CycleDaysID;
                        tempSchedule.Day = allClasses.ElementAt(i).Day;
                        tempSchedule.EndTime = allClasses.ElementAt(i).EndTime;
                        //tempSchedule.EndTimeRaw = new System.DateTime(1, 1, 1, 0, allClasses.ElementAt(i)JVEndTimeRaw, 0);
                        tempSchedule.Faculty = allClasses.ElementAt(i).Faculty;
                        tempSchedule.Room = allClasses.ElementAt(i).Room;
                        tempSchedule.Semester = allClasses.ElementAt(i).Semester;
                        tempSchedule.StartTime = allClasses.ElementAt(i).StartTime;
                        // tempSchedule.StartTimeRaw = new System.DateTime(1, 1, 1, 0, allClasses.ElementAt(i)JVStartTimeRaw, 0);
                        temp.Add(tempSchedule);
                    }
                }
                else
                {
                    if (allClasses.ElementAt(i).Semester.ToLower().Equals("spring"))
                    {
                        tempSchedule = new GetStudentScheduleBySemester_Result();
                        tempSchedule.BlockColor = allClasses.ElementAt(i).BlockColor;
                        tempSchedule.BlockColorRGB = allClasses.ElementAt(i).BlockColorRGB;
                        tempSchedule.ClassName = allClasses.ElementAt(i).ClassName;
                        tempSchedule.Course = allClasses.ElementAt(i).Course;
                        tempSchedule.CourseSection = allClasses.ElementAt(i).CourseSection;
                        tempSchedule.CycleDaysID = allClasses.ElementAt(i).CycleDaysID;
                        tempSchedule.Day = allClasses.ElementAt(i).Day;
                        tempSchedule.EndTime = allClasses.ElementAt(i).EndTime;
                        //tempSchedule.EndTimeRaw = new System.DateTime(1, 1, 1, 0, allClasses.ElementAt(i)JVEndTimeRaw, 0);
                        tempSchedule.Faculty = allClasses.ElementAt(i).Faculty;
                        tempSchedule.Room = allClasses.ElementAt(i).Room;
                        tempSchedule.Semester = allClasses.ElementAt(i).Semester;
                        tempSchedule.StartTime = allClasses.ElementAt(i).StartTime;
                        // tempSchedule.StartTimeRaw = new System.DateTime(1, 1, 1, 0, allClasses.ElementAt(i)JVStartTimeRaw, 0);
                        temp.Add(tempSchedule);
                    }
                }
            }

            return temp;

        }
    }
}
