﻿using API_Library_2._0.Final_Models;
using API_Library_2._0.Translation_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class GoogleAdminUsers
    {

        public static HttpBasicAuthenticator auth = new HttpBasicAuthenticator("jake.vossen@nextworld.net", "gmAHF7eS9!BI$20MzFkk0nW3qz&LdXykwUs!RSBq");
        public static  List<StudentModel> GetStudents()
        {
            List<StudentModel> temp = new List<StudentModel>();

            StudentNW.RootObject rootStudents = getRootStudents("https://master-api1.nextworld.net/v2/JVAdminGetStudents");

            for (int i = 0; i < rootStudents.Data.records.Count; i++)
            {
                temp.Add(convertStudentsToOldMoodel(rootStudents.Data.records.ElementAt(i).appData));
            }

            return temp;
        }

        private static StudentModel convertStudentsToOldMoodel(StudentNW.AppData appData)
        {
            StudentModel temp = new StudentModel();
            temp.EA7RecordsID = appData.JVEA7RecordsID;
            temp.FirstName = appData.JVFirstName;
            temp.Gender = appData.JVGender;
            temp.GradeLevel = appData.JVGradeLevel;
            temp.GroupEmail = appData.JVGroupEmail;
            temp.LastName = appData.JVLastName;
            temp.MiddleName = appData.JVMiddleName;
            temp.NickName = appData.JVNickName;
            temp.OrgUnitPath = appData.JVOrgUnitPath;
            temp.Password = appData.JVPassword;
            temp.StudentEmail = appData.JVEmail;
            temp.StudentID = appData.JVUserDefinedID;
            temp.Title = appData.JVTitle;
            return temp;
        }

        private static StudentNW.RootObject getRootStudents(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            client.Authenticator = auth;

            StudentNW.RootObject root = new StudentNW.RootObject();

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            root = JsonConvert.DeserializeObject<StudentNW.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return root;
        }

        public static List<FacultyStaffModel> GetFaculty()
        {

            List<FacultyStaffModel> temp = new List<FacultyStaffModel>();

            FacultyStaffNW.RootObject rootFacultyStaff = new FacultyStaffNW.RootObject();

            rootFacultyStaff = getRootFaculty("https://master-api1.nextworld.net/v2/JVAdminFacultyStaff?nwFilter=%7B%22$and%22:%5B%7B%22JVGroupEmail%22:%7B%22$like%22:%22faculty@valorchristian.com%22%7D%7D%5D%7D"); // This is different then the schoology one because it needs some other fields

            for (int i = 0; i < rootFacultyStaff.Data.records.Count; i++)
            {
                temp.Add(convertFacultyStaffToOldModel(rootFacultyStaff.Data.records.ElementAt(i).appData));
            }

            return temp;

        }

        private static FacultyStaffModel convertFacultyStaffToOldModel(FacultyStaffNW.AppData appData)
        {
            FacultyStaffModel temp = new FacultyStaffModel();
            temp.EA7RecordsID = appData.JVEA7RecordsID;
            temp.Email = appData.JVEmail;
            temp.FacultyID = appData.JVSchoolIDForFacutly;
            temp.FirstName = appData.JVFirstName;
            temp.GroupEmail = appData.JVGroupEmail;
            temp.LastName = appData.JVLastName;
            temp.OrgUnitPath = appData.JVOrgUnitPath;
            temp.Password = appData.JVPassword;
            return temp;
        }

        private static FacultyStaffNW.RootObject getRootFaculty(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            client.Authenticator = auth;

            FacultyStaffNW.RootObject root = new FacultyStaffNW.RootObject();

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            root = JsonConvert.DeserializeObject<FacultyStaffNW.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return root;
        }

        public static List<FacultyStaffModel> GetStaff()
        {
            List<FacultyStaffModel> temp = new List<FacultyStaffModel>();

            FacultyStaffNW.RootObject rootFacultyStaff = new FacultyStaffNW.RootObject();

            rootFacultyStaff = getRootFaculty("https://master-api1.nextworld.net/v2/JVAdminFacultyStaff?nwFilter=%7B%22$and%22:%5B%7B%22JVGroupEmail%22:%7B%22$like%22:%22info@valorlists.com%22%7D%7D%5D%7D"); // This is different than the schoology one because it needs a couple different paramaters

            for (int i = 0; i < rootFacultyStaff.Data.records.Count; i++)
            {
                temp.Add(convertFacultyStaffToOldModel(rootFacultyStaff.Data.records.ElementAt(i).appData));
            }

            return temp;
        }


    }
}
