﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class SchoologyMyValor
    {
        //authentication client
        HttpBasicAuthenticator authentication = new HttpBasicAuthenticator("Cheyanne.Miller@nextworld.net", "Pion33rs!");

        public SchoologyMyValor(){}

        public List<GetParentStudentDirectory_Result> getPSDirectory()
        {
            List<GetParentStudentDirectory_Result> list = new List<GetParentStudentDirectory_Result>();
            StudentRecord.RootObject getRoot = new StudentRecord.RootObject();

            //Get request to retrieve data from Nextworld
            var client = new RestClient("https://master-api1.nextworld.net/v2/CMGetParentStudentDirectory");
            client.Authenticator = authentication;

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<StudentRecord.RootObject>(response.Content);

            //translate data into appropriate model
            list = toDirectory(getRoot.Data.records);

            return list;
        }

        public List<GetParentStudentDirectory_Result> toDirectory(List<StudentRecord.Record> toTranslate)
        {
            List<GetParentStudentDirectory_Result> list = new List<GetParentStudentDirectory_Result>();
            GetParentStudentDirectory_Result student;
            StudentRecord.AppData s1;

            for(int i = 0; i < toTranslate.Count(); i++)
            {
                student = new GetParentStudentDirectory_Result();
                s1 = toTranslate.ElementAt(i).appData;

                //assign the data from the old model to the elements in the correct model
                student.LastName = s1.CMLastName;
                student.FirstName = s1.CMFirstName;
                student.MIddleName = s1.CMMiddleName;
                student.DisplayName = s1.CMDisplayName;

                student.StudentEmail = s1.CMStudentEmail;
                student.StudentID = s1.CMStudentID;
                student.Siblings = s1.CMSiblings;
                student.GradeLevel = s1.CMGradeLevel;

                student.Parents1 = s1.CMParents1;
                student.Parents2 = s1.CMParents2;
                student.HomePhone1 = s1.CMHomePhone1;
                student.HomePhone2 = s1.CMHomePhone2;
                student.AddressBlock1 = s1.CMAddressBlock1;
                student.AddressBlock2 = s1.CMAddressBlock2;
                student.CityStatePostBlock1 = s1.CMCityStatePostBlock1;
                student.CityStatePostBlock2 = s1.CMCityStatePostBlock2;

                list.Add(student);
            }

            return list;
        }
    }
}
