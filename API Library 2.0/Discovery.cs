﻿using API_Library_2._0.Final_Models;
using API_Library_2._0.Translation_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class Discovery
    {
        public static HttpBasicAuthenticator auth = new HttpBasicAuthenticator("jake.vossen@nextworld.net", "gmAHF7eS9!BI$20MzFkk0nW3qz&LdXykwUs!RSBq");
        public static TotalFund GetSolicitorData(int sid)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVTotalFund?nwFilter=%7B%22$and%22:%5B%7B%22JVSolicitorID%22:%7B%22$gte%22:" + sid + "%7D%7D,%7B%22JVSolicitorID%22:%7B%22$lte%22:" + sid + "%7D%7D%5D%7D");
            client.Authenticator = auth;

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWTotalFund.RootObject root = JsonConvert.DeserializeObject<NWTotalFund.RootObject>(response.Content);

            return convertToTotalFund(root);

        }

        private static TotalFund convertToTotalFund(NWTotalFund.RootObject root)
        {
            TotalFund t = new TotalFund();
            if (root.Data.records.Count > 0)
            {


                t.funds = (decimal)root.Data.records.ElementAt(0).appData.JVFunds;
                t.guid = Guid.Parse(root.Data.records.ElementAt(0).appData.nwId);
                t.id = root.Data.records.ElementAt(0).appData.JVTotalFundID;
                t.name = root.Data.records.ElementAt(0).appData.JVNickName;
                t.notes = root.Data.records.ElementAt(0).appData.Notes;
                t.solicitorId = root.Data.records.ElementAt(0).appData.JVSolicitorID;

            }
            return t;
        }


        public static List<SolicitorInfo2> GetSolicitorDataForFund(int sid, string fid)
        {
            List<SolicitorInfo2> temp = new List<SolicitorInfo2>();
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVGetSolicitorDataForFund?nwFilter=%7B%22$and%22:%5B%7B%22JVSolicitorID%22:%7B%22$gte%22:" + sid + "%7D%7D,%7B%22JVSolicitorID%22:%7B%22$lte%22:" + sid + "%7D%7D,%7B%22JVFundID%22:%7B%22$gte%22:" + fid + "%7D%7D,%7B%22JVFundID%22:%7B%22$lte%22:" + fid + "%7D%7D%5D%7D&nwSort=JVFundID");
            client.Authenticator = auth;
            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWSolicitorInfo2.RootObject root = JsonConvert.DeserializeObject<NWSolicitorInfo2.RootObject>(response.Content);

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                temp.Add(convertToSolicitorInfo2(root.Data.records.ElementAt(i)));
            }

            return temp;
        }

        private static SolicitorInfo2 convertToSolicitorInfo2(NWSolicitorInfo2.Record record)
        {
            SolicitorInfo2 temp = new SolicitorInfo2();
            temp.Address = record.appData.Address.AddressFull;
            temp.AmountAnonymous = Convert.ToInt32(record.appData.JVAmountAnonymous);
            temp.CityStateZip = record.appData.JVCityStateZip;
            temp.Contributor = record.appData.JVContributor;
            temp.ContributorNotes = record.appData.Notes;
            temp.CreatedOnDate = record.appData.DateTime;
            temp.DonorAnonymous = Convert.ToInt32(record.appData.JVDonorAnonymous);
            temp.Email = record.appData.JVEmail;
            temp.ExperienceDescription = record.appData.Description;
            temp.FirstName = record.appData.JVFirstName;
            temp.FundId = record.appData.JVFundID;
            temp.GiftSolicitorId = record.appData.JVSolicitorID;
            temp.GiftTotal = (decimal)record.appData.JVGiftTotal;
            temp.LastName = record.appData.JVLastName;
            temp.LineItemId = record.appData.JVLineItemID;
            temp.Phone = record.appData.PhoneNumber;
            temp.TotalFunds = (decimal)record.appData.JVGiftTotal;

            return temp;
        }

        public static List<SolicitorInfo2> GetSolicitorDetailsForFunds(int sid)
        {
            List<SolicitorInfo2> temp = new List<SolicitorInfo2>();
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVGetSolicitorDataForFund?nwFilter=%7B%22$and%22:%5B%7B%22JVSolicitorID%22:%7B%22$gte%22:" + sid + "%7D%7D,%7B%22JVSolicitorID%22:%7B%22$lte%22:" + sid + "%7D%7D%5D%7D&nwSort=JVFundID");
            client.Authenticator = auth;
            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWSolicitorInfo2.RootObject root = JsonConvert.DeserializeObject<NWSolicitorInfo2.RootObject>(response.Content);

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                temp.Add(convertToSolicitorInfo2(root.Data.records.ElementAt(i)));
            }

            return temp;
        }
        public static List<SolicitorInfo2> GetLeaderFundData(int lid, string fid)
        {
            List<SolicitorInfo2> temp = new List<SolicitorInfo2>();
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVGetSolicitorDataForFund?nwFilter=%7B%22$and%22:%5B%7B%22JVLeaderID%22:%7B%22$gte%22:"+lid+"%7D%7D,%7B%22JVLeaderID%22:%7B%22$lte%22:"+lid+"%7D%7D,%7B%22JVFundID%22:%7B%22$gte%22:"+fid+"%7D%7D,%7B%22JVFundID%22:%7B%22$lte%22:"+fid+"%7D%7D%5D%7D");
            client.Authenticator = auth;
            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWSolicitorInfo2.RootObject root = JsonConvert.DeserializeObject<NWSolicitorInfo2.RootObject>(response.Content);

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                temp.Add(convertToSolicitorInfo2(root.Data.records.ElementAt(i)));
            }

            return temp;
        }



        public static List<LeaderFundDetail> GetLeaderData(int lid, int fid)
        {
            List<LeaderFundDetail> temp = new List<LeaderFundDetail>();

            var client = new RestClient("https://master-api1.nextworld.net/v2/JVLeaderFundDetail?nwFilter=%7B%22$and%22:%5B%7B%22JVLeaderID%22:%7B%22$gte%22:" + lid + "%7D%7D,%7B%22JVLeaderID%22:%7B%22$lte%22:" + lid + "%7D%7D%5D%7D");
            client.Authenticator = auth;
            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWLeaderFundDetail.RootObject root = JsonConvert.DeserializeObject<NWLeaderFundDetail.RootObject>(response.Content);

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                temp.Add(convertToLeaderData(root.Data.records.ElementAt(i).appData));
            }

            return temp;
        }

        private static LeaderFundDetail convertToLeaderData(NWLeaderFundDetail.AppData appData)
        {
            LeaderFundDetail temp = new LeaderFundDetail();

            temp.Address = appData.Address.AddressFull;
            temp.AmountAnonymous = Convert.ToInt16(appData.JVAmountAnonymous);
            temp.CityStateZip = appData.JVCityStateZip;
            temp.Contributor = appData.JVContributor;
            temp.ContributorNotes = appData.Notes;
            temp.CreatedOnDate = appData.nwCreatedDate;
            temp.DonorAnonymous = Convert.ToInt16(appData.JVDonorAnonymous);
            temp.Email = appData.JVEmail;
            temp.ExperienceDescription = appData.Description;
            temp.ExperienceId = appData.JVExperienceID;
            temp.GiftTotal = (decimal)appData.JVGiftTotal;
            temp.LeaderId = appData.JVLeaderID;
            temp.LineItemId = appData.JVLineItemID;
            temp.Phone = appData.PhoneNumber;
            temp.SolicitorFirstName = appData.Name.NameName.Substring(0, appData.Name.NameName.IndexOf(" "));
            temp.SolicitorLastName = appData.Name.NameName.Substring(appData.Name.NameName.IndexOf(" "));
            temp.TotalFunds = (decimal)appData.JVGiftTotal;
            return temp;
        }

        public static List<SolicitorFunds> GetSolicitorFunds(int sid)
        {
            List<SolicitorFunds> temp = new List<SolicitorFunds>();

            var client = new RestClient("https://master-api1.nextworld.net/v2/JVSolicitorFunds?nwFilter=%7B%22$and%22:%5B%7B%22JVSolicitorID%22:%7B%22$gte%22:"+sid+"%7D%7D,%7B%22JVSolicitorID%22:%7B%22$lte%22:"+sid+"%7D%7D%5D%7D");
            client.Authenticator = auth;
            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWSolicitorFunds.RootObject root = JsonConvert.DeserializeObject<NWSolicitorFunds.RootObject>(response.Content);

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                temp.Add(convertToSolicitorFunds(root.Data.records.ElementAt(i).appData));
            }

            return temp;


        }

        private static SolicitorFunds convertToSolicitorFunds(NWSolicitorFunds.AppData appData)
        {
            SolicitorFunds temp = new SolicitorFunds();
            temp.Description = appData.Description;
            temp.FundId = appData.JVFundID;
            temp.SolicitorId = appData.JVSolicitorID;
            return temp;
        }

        public static List<LeaderFunds> GetLeaderStatus(int lid)
        {
            List<LeaderFunds> temp = new List<LeaderFunds>();

            var client = new RestClient("https://master-api1.nextworld.net/v2/JVLeaderFunds?nwFilter=%7B%22$and%22:%5B%7B%22JVLeaderID%22:%7B%22$gte%22:"+lid+"%7D%7D,%7B%22JVLeaderID%22:%7B%22$lte%22:"+lid+"%7D%7D%5D%7D");

            client.Authenticator = auth;
            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWLeaderFunds.RootObject root = JsonConvert.DeserializeObject<NWLeaderFunds.RootObject>(response.Content);

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                temp.Add(convertToLeaderFunds(root.Data.records.ElementAt(i)));
            }

            return temp;
        }

        private static LeaderFunds convertToLeaderFunds(NWLeaderFunds.Record record)
        {
            LeaderFunds temp = new LeaderFunds();

            temp.ExperienceDescription = record.appData.Description;
            temp.FundId = record.appData.JVFundID;
            temp.LeaderId = record.appData.JVLeaderID;

            return temp;
        }

        public static List<AdminFundDetail> GetAdminDetail(string start, string end)
        {
            List<AdminFundDetail> temp = new List<AdminFundDetail>();

            var client = new RestClient("https://master-api1.nextworld.net/v2/JVAdminFundDetail?nwFilter=%7B%22$and%22:%5B%7B%22StartDate%22:%7B%22$lte%22:%22" + end + "18%22%7D%7D,%7B%22StartDate%22:%7B%22$gte%22:%22" + start + "%22%7D%7D%5D%7D");
            client.Authenticator = auth;
            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWAdminFundDetail.RootObject root = JsonConvert.DeserializeObject<NWAdminFundDetail.RootObject>(response.Content);


            for (int i = 0; i < root.Data.records.Count; i++)
            {
                temp.Add(convertToAdminFundDetail(root.Data.records.ElementAt(i).appData));
            }
            return temp;
        }

        private static AdminFundDetail convertToAdminFundDetail(NWAdminFundDetail.AppData appData)
        {
            AdminFundDetail temp = new AdminFundDetail();

            temp.Address = appData.Address.AddressFull;
            temp.CheckDate = appData.StartDate;
            temp.CheckNumber = ""+ appData.CheckNumber;
            temp.City = appData.Address.AddressCity;
            temp.CityStateZip = appData.JVCityStateZip;
            temp.Contributor = appData.JVContributor;
            temp.ContributorNotes = appData.Notes;
            temp.CreatedOnDate = "" + appData.nwCreatedDate;
            temp.ExperienceDescription = appData.Description;
            temp.FirstName = appData.JVFirstName;
            temp.FundId = appData.JVFundID;
            temp.FundSplitAmount = (decimal)appData.JVFundSplitAmount;
            temp.GiftDate = appData.JVGiftDate;
            temp.GiftId = appData.JVGiftID;
            temp.GiftPaymentType = appData.PaymentMethod;
            temp.GiftReference = appData.JVGiftReference;
            temp.GiftReferenceDate = appData.JVGiftReferenceDate;
            temp.GiftSolicitorFirstName = appData.JVFirstName;
            temp.GiftSolicitorId = "" + appData.JVSolicitorID;
            temp.GiftSolicitorLastName = appData.JVLastName;
            temp.GiftSubType = appData.JVGiftSubType;
            temp.GiftType = appData.JVGiftType;
            temp.LastName = appData.JVLastName;
            temp.LineItemId = appData.JVLineItemID;
            temp.Name = appData.Name.NameName;
            temp.Phone = appData.PhoneNumber;
            temp.PrimaryEmail = appData.JVEmail;
            temp.SecondaryEmail = appData.JVSecondaryEmail;
            temp.State = appData.Address.AddressState;
            temp.ZipCode = appData.JVAddressZip;

            return temp;
        }
    }


}
