﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaModel
{
    public class CMStudentGradesModel
    {
        public class ZnwWorkflowInstance
        {
        }

        public class AppData
        {
            //application attributes
            public string CMFirstName { get; set; }
            public string CMLastName { get; set; }
            public string CMStudentIDNumber { get; set; }
            public string CMGradeLevel { get; set; }
            public int CMEA7RecordsID { get; set; }
            public double CMGrade { get; set; }
            public string CMCourse { get; set; }
            public string CMCourseSection { get; set; }
            public string CMClassName { get; set; }
            public string CMSectionCode { get; set; }
            public string CMRoom { get; set; }
            public string CMFaculty { get; set; }

            //Nextbox stuff
            public object nwId { get; set; }
            public object nwExternalId { get; set; }
            public object nwCreatedByUser { get; set; }
            public object nwCreatedDate { get; set; }
            public object nwLastModifiedByUser { get; set; }
            public string nwLastModifiedDate { get; set; }
            public object nwTenantStripe { get; set; }
            public object AttachmentGroupId { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public object znwOwner { get; set; }
            public bool znwLocked { get; set; }
            public object znwSparseOwner { get; set; }
            public object nwNateId { get; set; }
            public object nwNateDisposition { get; set; }
            public object InternalSeq { get; set; }
            public object nwBaseVersion { get; set; }
            public string nwSubTableRecordId { get; set; }
        }

        public class Record
        {
            public AppData appData { get; set; }
            public string version { get; set; }
            public string nateDisposition { get; set; }
        }

        public class ClientState
        {
            public object RootNwId { get; set; }
            public string SaveType { get; set; }
            public string ApplicationName { get; set; }
        }

        public class RootObject
        {
            public List<Record> records { get; set; }
            public ClientState clientState { get; set; }
            public bool createContainer { get; set; }
            public bool commitContainer { get; set; }
        }
    }
}
